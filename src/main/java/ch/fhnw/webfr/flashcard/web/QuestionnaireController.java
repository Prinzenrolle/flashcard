package ch.fhnw.webfr.flashcard.web;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.naming.Context;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ch.fhnw.webfr.flashcard.domain.Questionnaire;
import ch.fhnw.webfr.flashcard.persistence.QuestionnaireRepository;


@Controller
@RequestMapping("/questionnaires")
public class QuestionnaireController {

    @Autowired
    private QuestionnaireRepository questionnaireRepository;

    @RequestMapping(method = RequestMethod.GET)
    public String findAll(Model model) throws IOException {
        model.addAttribute("questionnaires", questionnaireRepository.findAll());
        return "questionnaires/list";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String findById(@PathVariable String id, Model model) throws IOException {
        // Bei Optional keine Null-Checker mehr. Man kann schön auf isPresent prüfen, um zu schauen, ob ein Objekt drin ist.
        Optional<Questionnaire> questionnaire = questionnaireRepository.findById(id);

        if (questionnaire.isPresent()) {
            model.addAttribute("questionnaire", questionnaire.get());
        } else {
            return "404";
        }

        return "questionnaires/show";
    }

    @RequestMapping(params = "form", method = RequestMethod.GET)
    public String form(Model model) {
        model.addAttribute("questionnaire", new Questionnaire());
        return "questionnaires/create";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String create(@Valid Questionnaire questionnaire, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "questionnaires/create";
        }
        questionnaireRepository.save(questionnaire);
        return "redirect:/questionnaires";
    }

    @RequestMapping(params = "update", value = "/{id}", method = RequestMethod.GET)
    public String update(Model model, @PathVariable String id) {
        model.addAttribute("questionnaire", questionnaireRepository.findById(id));
        return "questionnaires/update";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public String delete(@PathVariable String id) {
        if (questionnaireRepository.existsById(id)) {
            questionnaireRepository.deleteById(id);
        } else {
            return "404";
        }
        return "redirect:/questionnaires";
    }

    @RequestMapping(method = RequestMethod.PUT)
    public String modify(@Valid Questionnaire questionnaire, BindingResult bindingResult) {
        Optional<Questionnaire> quest = questionnaireRepository.findById(questionnaire.getId());
        if (bindingResult.hasErrors()) {
            return "questionnaires/update";
        }
        if (quest.isPresent()) {
            System.out.println(questionnaire.getId());
            quest.get().setTitle(questionnaire.getTitle());
            quest.get().setDescription(questionnaire.getDescription());
            questionnaireRepository.save(quest.get());
        }
        return "redirect:/questionnaires";
    }
}