package ch.fhnw.webfr.flashcard.web;


import ch.fhnw.webfr.flashcard.domain.Questionnaire;
import ch.fhnw.webfr.flashcard.persistence.QuestionnaireRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/*
@Controller
@RequestMapping("hello")
public class HelloWorldController {

    // Anschliessen der MongoDB
    @Autowired
    private QuestionnaireRepository questionnaireRepository;

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    String sayHello(@RequestParam("firstname") String firstname) {
        List<Questionnaire> questionnaires = questionnaireRepository.findAll();
        String response = "Hello " + firstname + "<br/>"
                + "You have " + questionnaires.size() + " Questionnaires in your repo.";
        return response;
    }
}*/
